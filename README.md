# Dotfiles for my [Swayfx](https://github.com/WillPower3309/swayfx) setup.


![ScreenShot](screenshot.jpg)

Current Wallpaper:
![Wallpaper](wallpaper.jpg)


## Details

Below is a list of some of the packages that I use for my current setup.<br />

- **OS** --- [Voidlinux](https://www.Voidlinux.org/)
- **WM** --- [Swayfx](https://github.com/WillPower3309/swayfx)
- **Status Bar** --- [Yambar](https://codeberg.org/dnkl/yambar)
- **Screen Locker** --- [Swaylock](https://github.com/swaywm/swaylock)
- **Screenshots** --- [Grim](https://aur.archlinux.org/packages/grim-git)
                  --- [Slurp](https://aur.archlinux.org/packages/slurp-git)
- **Idle Management Daemon** --- [Swayidle](https://aur.archlinux.org/packages/swayidle.git)
- **Shell** --- [Fish](https://fishshell.com/) using [Starship](https://starship.rs/) 
- **Terminal** --- [Foot](https://codeberg.org/dnkl/foot) & Kitty
- **Notification Daemon** --- [Mako](https://github.com/emersion/mako) 
- **Application Launcher** --- [Fuzzel](https://codeberg.org/dnkl/fuzzel)
- **Image Viewer** --- [Imv](https://sr.ht/~exec64/imv/)
- **Editor** --- [Neovim](https://neovim.io/)
- **Web Browser** --- [Qutebrowser](https://www.qutebrowser.org) & [Firefox]
- **PDF Viewer** --- [Zathura](https://pwmt.org/projects/zathura/)
- **Video player** --- [Mpv](https://mpv.io/)



