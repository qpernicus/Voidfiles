#!/bin/bash

void="$(xbps-install --memory-sync --dry-run --update | grep -Fe update -e install | wc -l)"
number=$((void))

if [ "$number" -gt 0 ]; then
	text=" $number"
else
	text=" "
fi

echo "{\"text\":\""$text"\"}"
exit 0
