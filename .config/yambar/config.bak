hack: &hack JuliaMono:pixelsize=13
awesome: &awesome Font Awesome 6 Free:style=solid:pixelsize=14
awesome_brands: &awesome_brands Font Awesome 6 Brands:pixelsize=16

std_underline: &std_underline {underline: { size: 1, color: ff0000ff}}
bg_default: &bg_default {stack: [{background: {color: 282a36ff}}, {underline: {size: 1, color: 8be9fdff}}]}
bar:
  height: 24
  location: top
  font: JuliaMono:pixelsize=10
  spacing: 2
  margin: 0
  layer: bottom
  foreground: eeeeeeff
  background: 2E3440dd

  center:
    - clock:
        content:
          - string: {text:  , font: "Font Awesome 6 Free:style=solid:size=12"}
          - string: {text: "{date}", right-margin: 5}
          - string: {text:  , font: "Font Awesome 6 Free:style=solid:size=12"}
          - string: {text: "{time}"}
  right:
    - removables:
        anchors:
          drive: &drive { text: , font: *awesome}
          optical: &optical {text: , font: *awesome}
        spacing: 5
        content:
          map:
            conditions:
              ~mounted:
                map:
                  on-click: udisksctl mount -b {device}
                  conditions:
                    ~optical: [{string: *drive}, {string: {text: "{label}"}}]
                    optical: [{string: *optical}, {string: {text: "{label}"}}]
              mounted:
                map:
                  on-click: udisksctl unmount -b {device}
                  conditions:
                    ~optical:
                      - string: {<<: *drive, deco: *std_underline}
                      - string: {text: "{label}"}
                    optical:
                      - string: {<<: *optical, deco: *std_underline}
                      - string: {text: "{label}"}
    - network:
        name: enp1s0
        content:
          map:
            conditions:
              ~carrier: {empty: {}}
              carrier:
                map:
                  default: {string: {text: , font: *awesome, foreground: ffffff66}}
                  conditions:
                    state == up && ipv4 != "": {string: {text: , font: *awesome}}
    - network:
        name: wlp2s0
        poll-interval: 1000
        content:
          map:
            default: {string: {text: , font: *awesome, foreground: ffffff66}}
            conditions:
              state == down: {string: {text: , font: *awesome, foreground: ff0000ff}}
              state == up:
                map:
                  default:
                    - string: {text: , font: *awesome}
                    - string: {text: " {ssid} "}

                  conditions:
                    ipv4 == "":
                      - string: {text: , font: *awesome, foreground: ffffff66}
                      - string: {text: " {ssid} ", foreground: ffffff66}

    - pipewire:
        anchors:
          volume: &volume
            conditions:
              muted: {string: {text: "󰝟 {cubic_volume}%", foreground: ff0000ff}}
              ~muted: {string: {text: " {cubic_volume}%"}}      
        content:
          map:
            conditions:
              type == "sink":
                map: 
                  <<: *volume  
    - label:
        content:
          string:
            on-click:  "foot -e sudo xbps-install -Su; pkill -SIGRTMIN+8 waybar && notify-send 'The system has been updated' "
            text: "Update(s)"    
    - battery:
        name: BAT0
        poll-interval: 30
        anchors:
          discharging: &discharging
            list:
              items:
                - ramp:
                    tag: capacity
                    items:
                      - string: {text: , foreground: ff0000ff, font: *awesome}
                      - string: {text: , foreground: ffa600ff, font: *awesome}
                      - string: {text: , font: *awesome}
                      - string: {text: , font: *awesome}
                      - string: {text: , font: *awesome}
                      - string: {text: , font: *awesome}
                      - string: {text: , font: *awesome}
                      - string: {text: , font: *awesome}
                      - string: {text: , font: *awesome}
                      - string: {text: , foreground: 00ff00ff, font: *awesome}
                - string: {text: "{capacity}% {estimate}"}
        content:
          map:
            conditions:
              state == unknown:
                <<: *discharging
              state == discharging:
                <<: *discharging
              state == charging:
                - string: {text: , foreground: 00ff00ff, font: *awesome}
                - string: {text: "{capacity}%"}
              state == full:
                - string: {text: , foreground: 00ff00ff, font: *awesome}
                - string: {text: "full"}
              state == "not charging":
                - ramp:
                    tag: capacity
                    items:
                      - string: {text:  , foreground: ff0000ff, font: *awesome}
                      - string: {text:  , foreground: ffa600ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                      - string: {text:  , foreground: 00ff00ff, font: *awesome}
                - string: {text: "{capacity}%"}
    - label:
        content:
          string:
            on-click: systemctl poweroff
            text: 
            font: *awesome
  left:
    - river:
        anchors:
          - base: &river_base
              left-margin: 10
              right-margin: 13 
              default: {string: {text: , font: *hack}}
              conditions:
                id == 1: {string: {text: 󰖟, font: *hack}}  
                id == 2: {string: {text: , font: *hack}}  
                id == 3: {string: {text: , font: *hack}}  
                id == 4: {string: {text: , font: *hack}}  
                id == 5: {string: {text: , font: *hack}}  
                id == 6: {string: {text: , font: *hack}}  
                id == 7: {string: {text: 7, font: *hack}} 
                id == 8: {string: {text: 8, font: *hack}}  
                id == 9: {string: {text: 9, font: *hack}}              
                id == 10: {string: {text: "scratchpad", font: *hack}}  
                id == 11: {string: {text: "work", font: *hack}}  

        content:
          map:
            on-click: 
              left: sh -c "riverctl set-focused-tags $((1 << ({id} - 1)))"
              right: sh -c "riverctl toggle-focused-tags $((1 << ({id} -1)))"
              middle: sh -c "riverctl toggle-view-tags $((1 << ({id} -1)))"
            conditions:
              state == urgent:
                map:
                  <<: *river_base
                  deco: {background: {color: D08770ff}}
              state == focused:
                map:
                  <<: *river_base
                  deco: *bg_default
              state == visible && ~occupied:
                map:
                  <<: *river_base
              state == visible && occupied:
                map:
                  <<: *river_base
                  deco: *bg_default
              state == unfocused:
                map:
                  <<: *river_base
              state == invisible && ~occupied: {empty: {}}
              state == invisible && occupied:
                map:
                  <<: *river_base
                  deco: {underline: {size: 1, color: FFA300ff}}
