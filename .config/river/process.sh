#!/bin/bash

# Authentication dialog

pkill -f /usr/libexec/polkit-gnome-authentication-agent-1
/usr/libexec/polkit-gnome-authentication-agent-1 &

pkill -f kanshi
kanshi &

pkill -f swaybg
swaybg -m fill -i ~/.cache/wallpaper &

pkill -f mako
mako &

#pkill -f waybar
#waybar -c ~/.config/waybar/river/config-river -s ~/.config/waybar/river/river_style.css &

pkill -f yambar
yambar

pkill -f wlsunset
wlsunset -l 52.9 -L 6.5 &

export wallpaper='~/.cache/wallpaper'

pkill -f swayidle
swayidle -w \
	timeout 300 'swaylock -f -i $wallpaper' \
	timeout 600 'wlopm --off \*;swaylock -F -i ~/.cache/wallpaper' resume 'wlopm --on \*' \
	before-sleep 'swaylock -f -i $wallpaper' &

nm-applet &
blueman-applet &
blueman-manager &
