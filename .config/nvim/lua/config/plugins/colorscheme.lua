return {
	"ellisonleao/gruvbox.nvim",
	priority = 1000,
	config = function()
		vim.g.edge_transparent_background = 1
		vim.cmd.colorscheme("gruvbox")
	end,
}

-- Edge theme
