colors = [
    ["#2e3440", "#2e3440"],  # background
    ["#d8dee9", "#d8dee9"],  # foreground
    ["#3b4252", "#3b4252"],  # background lighter
    ["#bf616a", "#bf616a"],  # red
    ["#a3be8c", "#a3be8c"],  # green
    ["#ebcb8b", "#ebcb8b"],  # yellow
    ["#81a1c1", "#81a1c1"],  # blue
    ["#b48ead", "#b48ead"],  # magenta
    ["#88c0d0", "#88c0d0"],  # cyan
    ["#e5e9f0", "#e5e9f0"],  # white
    ["#4c566a", "#4c566a"],  # grey
    ["#d08770", "#d08770"],  # orange
    ["#8fbcbb", "#8fbcbb"],  # super cyan
    ["#5e81ac", "#5e81ac"],  # super blue
    ["#242831", "#242831"],  # super dark background
    ["#282a36", "#282a36"],	 # Dracula Background Color
    ["#44475a", "#44475a"],	 # Dracula Current Line Color
    ["#44475a", "#44475a"],  # Dracula Selection Color
    ["#f8f8f2", "#f8f8f2"],  # Dracula Foreground Color
    ["#6272a4", "#6272a4"],  # Dracula Comment Color
    ["#8be9fd", "#8be9fd"],  # Dracula Cyan Color
    ["#50fa7b", "#50fa7b"],  # Dracula Green Color
    ["#ffb86c", "#ffb86c"],  # Dracula Orange Color
    ["#ff79c6", "#ff79c6"],  # Dracula Pink Color
    ["#bd93f9", "#bd93f9"],  # Dracula Purple Color
    ["#ff5555", "#ff5555"],  # Dracula Red Color
    ["#f1fa8c", "#f1fa8c"],  # Dracula Yellow Color
]


# colors = [#["#282c34", "#282c34"],
#           ["000000AA", "#000000AA"],
#           ["#1c1f24", "#1c1f24"],
#           ["#dfdfdf", "#dfdfdf"],
#           ["#ff6c6b", "#ff6c6b"],
#           ["#98be65", "#98be65"],
#           ["#da8548", "#da8548"],
#           ["#51afef", "#51afef"],
#           ["#c678dd", "#c678dd"],
#           ["#46d9ff", "#46d9ff"],
#           ["#a9a1e1", "#a9a1e1"]]
