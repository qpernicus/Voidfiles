#! /bin/sh

cmd_exist() {
	unalias "$1" >/dev/null 2>&1
	command -v "$1" >/dev/null 2>&1
}
__kill() { kill -9 "$(pidof "$1")" >/dev/null 2>&1; }
__start() { sleep 1 && "$@" >/dev/null 2>&1 & }
__running() { pidof "$1" >/dev/null 2>&1; }

# # Session setup
#systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
#dbus-update-activation-environment --systemd \
#	WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=qtile

#Xorg Background
# if cmd_exist feh ; then
#     __kill feh
#     __start /home/jeroen/.fehbg
# fi
#Wayland Background
if cmd_exist swaybg; then
	__kill swaybg
	__start swaybg -m fill -i ~/.cache/wallpaper
fi

# Apps to autostart

#run nm-applet &
#exec /usr/bin/picom --experimental-backend &
blueman-manager &
blueman-applet &
nm-applet &
#numlockx on &
#/usr/bin/emacs --daemon &
thunderbird &

# # wlsunset

if cmd_exist wlsunset; then
	__kill wlsunset
	__start wlsunset -l 52.9 -L 6.5
fi

# Authentication dialog

if [ -f /usr/libexec/polkit-gnome-authentication-agent-1 ]; then
	__kill polkit-gnome-authentication-agent-1
	__start /usr/libexec/polkit-gnome-authentication-agent-1
fi

# Notification daemon

# if cmd_exist dunst ; then
#     __kill dunst
#     __start dunst
# fi

if cmd_exist mako; then
	__kill mako
	__start mako
fi
# # Swayidle daemon

if cmd_exist swayidle; then
	__kill swayidle
	__start swayidle -w \
		timeout 300 'swaylock -f -i /home/jeroen/.cache/wallpaper' \
		timeout 600 'wlopm --off \*;swaylock -F -i ~/.cache/wallpaper' resume 'wlopm --on \*' \
		before-sleep 'swaylock -f -i /home/jeroen/.cache/wallpaper' &

fi
