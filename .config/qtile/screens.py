import os
import subprocess

from libqtile.config import (
    # KeyChord,
    # Key,
    Screen,
    # Group,
    # Drag,
    # Click,
    # ScratchPad,
    # DropDown,
    # Match,
)

# from libqtile.command import lazy
from libqtile import bar, widget  # , hook, layout
# from libqtile.lazy import lazy
from libqtile import qtile
from colors import colors
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration


#terminal = "kitty"
terminal = "foot"


def open_pavu():
    qtile.cmd_spawn("pavucontrol")


def open_network():
    qtile.cmd_spawn("nm-connection-editor")


def open_blueman():
    qtile.cmd_spawn("blueman-manager")

def longNameParse(text): 
    for string in ["Chromium", "Firefox", "Brave"]: #Add any other apps that have long names here
        if string in text:
            text = string
        else:
            text = text
    return text

# group_box_settings = {
#    "active":"e5e9f0",  #"#9ccfd8",#"#31748f",#"#96CDFB",
#    "inactive":"#6E6C7E",
#    "foreground":"#905aff", #"#96CDFB",
#    #"foreground":"#00000000",
#    "background":"#000000AA", 
#    "rounded":False,
#    "borderwidth":0,
#    "margin":4,
#    "highlight_method":"text",
#    "highlight_color":"#905aff", #"#e5e9f0", #"#26233a", 
#    "other_screen_border":"#6E6C7E",
#    "other_current_screen_border":"#96CDFB",
#    "this_current_screen_border":"#96CDFB",#"#96CDFB",Geselecteerde scherm
#    "this_screen_border":"#6E6C7E",
# }
                    

text_size = 11
icon_size = 11

widget_defaults = dict(
    font= "Ubuntu Mono", #font="FiraCode Nerd Font Regular",
    fontsize="11",
    padding=5,
)


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    linewidth=0,
                    foreground=colors[2],
                    padding=5,
                    size_percent=40,
                ),
                widget.TextBox(
                        text = '|',
                        font = "Ubuntu Mono",
                        background = colors[0],
                        foreground = '474747',
                        padding = 2,
                        fontsize = 14
                        ),  
                widget.Sep(
                    linewidth=0,
                    foreground=colors[2],
                    padding=5,
                    size_percent=40,
                ),                                       
                # widget.GroupBox(
                #     #font="Font Awesome 5 Free Solid",
                #     font= "Roboto Bold",
                #     #**group_box_settings,
                #     fontsize=11,
                # ),
                widget.GroupBox(
                        font = "Ubuntu Bold",
                        fontsize = 9,
                        margin_y = 3,
                        margin_x = 0,
                        padding_y = 5,
                        padding_x = 3,
                        borderwidth = 3,
                        active = colors[6],
                        inactive = colors[2],
                        rounded = False,
                        highlight_color = colors[2],
                        highlight_method = "line",
                        this_current_screen_border = colors[6],
                        this_screen_border = colors [4],
                        other_current_screen_border = colors[6],
                        other_screen_border = colors[4],
                        foreground = colors[2],
                        background = colors[0]
                        ),                
                widget.Sep(
                    linewidth=0,
                    foreground=colors[2],
                    padding=10,
                    size_percent=40,
                ),      
                widget.TextBox(
                        text = '|',
                        font = "Ubuntu Mono",
                        background = colors[0],
                        foreground = '474747',
                        padding = 2,
                        fontsize = 14
                        ),                         
                widget.Sep(
                    linewidth=0,
                    foreground=colors[2],
                    padding=250,
                    size_percent=50,
                ),
                widget.Spacer(),
                widget.WindowName(
                        foreground = colors[2],
                        background = colors[0],
                        fontsize=12,
                        padding = 0
                        ),                
                # widget.WindowName(
                #     foreground="#d9e0ee",
                #     #**widget_defaults,
                #     font="Roboto",
                #     fontsize=12,
                #     parse_text=longNameParse,
                #     #padding= 0,
                #     max_chars=50
                #     ),                
                widget.Spacer(),
                widget.Sep(
                    linewidth=0,
                    padding=5,
                    size_percent=50,
                ),
                widget.TextBox(
                        text = '|',
                        font = "Ubuntu Mono",
                        background = colors[0],
                        foreground = '474747',
                        padding = 2,
                        fontsize = 14
                        ),             
                widget.CheckUpdates(
                    **widget_defaults,
                    update_interval=600,
                    distro='Void',
                    #custom_command='~/.local/bin/statusbar/void-updates.sh',
                    display_format=' {updates}',
                    foreground = colors[4],
                    colour_have_updates = colors[4],
                    colour_no_updates = colors[5], 
                    execute='foot -e sudo xbps-install -Suv',
                    decorations=[
                        BorderDecoration(
                            colour = colors[4],
                            border_width = [0, 0, 2, 0],
                            padding_x = 5,
                            padding_y = None,
                        )
                    ],                    
                ),                             
                # widget.CheckUpdates(
                #     **widget_defaults,
                #     update_interval=1800,
                #     distro='Arch_yay',
                #     custom_command='~/.local/bin/statusbar/waybar-updates.sh',
                #     display_format=' {updates}',
                #     foreground = colors[4],
                #     colour_have_updates = colors[4],
                #     colour_no_updates = colors[5],                    
                #     execute='foot -e paru',
                #     decorations=[
                #         BorderDecoration(
                #             colour = colors[4],
                #             border_width = [0, 0, 2, 0],
                #             padding_x = 5,
                #             padding_y = None,
                #         )
                #     ],
                # ),             
                widget.Sep(
                    linewidth=0,
                    padding=5,
                    size_percent=50,
                ),                             
                widget.GenPollText(
                    update_interval=1, 
                    **widget_defaults, 
                    func=lambda: subprocess.check_output(os.path.expanduser("~/.local/bin/statusbar/volumecontrol")).decode(), 
                    mouse_callbacks={'Button1': lambda: qtile.spawn(os.path.expanduser("~/.local/bin/statusbar/volumecontrol down"), 
                    shell=True), 
                    'Button2': lambda: qtile.spawn(os.path.expanduser("~/.local/bin/statusbar/volumecontrol mute"), 
                    shell=True), 'Button3': lambda: qtile.spawn(os.path.expanduser("~/.local/bin/statusbar/volumecontrol up"), 
                    shell=True)},
                    foreground = colors[6],
                    background = colors[0],
                    decorations=[
                        BorderDecoration(
                            colour = colors[6],
                            border_width = [0, 0, 2, 0],
                            padding_x = 5,
                            padding_y = None,
                        )
                    ],                    
                ),
                widget.Sep(
                    linewidth=0,
                    padding=5,
                    size_percent=50,
                ),
                widget.Wlan(
                    disconnected_message="Wloss",
                    fontsize=text_size,
                    foreground=colors[5],
                    interface="wlp2s0",
                    mouse_callbacks={"Button1": open_network},
                    padding=5,
                    format=" : {essid}",
                    decorations=[
                        BorderDecoration(
                            colour = colors[5],
                            border_width = [0, 0, 2, 0],
                            padding_x = 5,
                            padding_y = None,
                        )
                    ],                    
                ),
                widget.Sep(
                    linewidth=0,
                    foreground=colors[1],
                    padding=5,
                    size_percent=50,
                ),
                widget.GenPollText(
                    update_interval=1, 
                    func=lambda: subprocess.check_output(os.path.expanduser("~/.local/bin/statusbar/battery.py")).decode(), mouse_callbacks={'Button1': lambda: qtile.spawn(os.path.expanduser("/home/jeroen/.local/bin/statusbar/battery.py --c left-click"), shell=True)},
                    foreground = colors[7],
                    background = colors[0],
                    padding = 5,
                    decorations=[
                        BorderDecoration(
                            colour = colors[7],
                            border_width = [0, 0, 2, 0],
                            padding_x = 5,
                            padding_y = None,
                        )
                    ],                    
                    ),
                widget.Sep(
                    linewidth=0,
                    foreground=colors[1],
                    padding=5,
                    size_percent=50,
                ),                 
                # widget.TextBox(
                #     text=" ",
                #     text=" ",
                #     text=" ",
                #     font="Font Awesome 5 Free Solid",
                #     fontsize=icon_size,
                #     foreground=colors[4],
                # ),
                widget.Clock(
                    #format="%d %b",
                    #        "format": "{:%A %d %B %R}"
                    format = "%A %d %B - %H:%M ",
                    foreground=colors[4],
                    fontsize=text_size,
                    padding = 5,
                    decorations=[
                        BorderDecoration(
                            colour = colors[4],
                            border_width = [0, 0, 2, 0],
                            padding_x = 5,
                            padding_y = None,
                        )
                    ],                    
                ),
                widget.Sep(
                    linewidth=0,
                    padding=5,
                    size_percent=50,
                ),                             
                widget.QuickExit(
                    default_text=" ",
                    font="Font Awesome 5 Free Solid",
                    padding=5,
                    #rounded=True,
                    # mouse_callbacks={
                    #     "Button1": lambda: qtile.cmd_spawn(
                    #         "sh /home/jeroen/.config/rofi/powermenu.sh"
                    #     )
                    # },
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn(
                            "/home/jeroen/.local/bin/powermenu.sh"
                        )
                    },                    
                    fontsize=13,
                    foreground = colors[6],
                    background = colors[0],
                    format = "%A, %B %d - %H:%M ",
                    decorations=[
                        BorderDecoration(
                            colour = colors[6],
                            border_width = [0, 0, 2, 0],
                            padding_x = 5,
                            padding_y = None,
                        )
                    ],                    
                ),                
                widget.Sep(
                    linewidth=0,
                    foreground=colors[1],
                    padding=5,
                    size_percent=50,
                ),     
                widget.TextBox(
                        text = '|',
                        font = "Ubuntu Mono",
                        background = colors[0],
                        foreground = '474747',
                        padding = 2,
                        fontsize = 14
                        ),                            
                widget.CurrentLayoutIcon(
                    custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                    foreground = colors[2],
                    background = colors[0],
                    padding = 0,
                    scale = 0.5
                ),     
                widget.CurrentLayout(
                        foreground = colors[6],
                        background = colors[0],
                        padding = 5
                        ),                           
                widget.Sep(
                    linewidth=0,
                    foreground=colors[1],
                    padding=5,
                    size_percent=50,
                ),
                widget.GenPollText(
                    update_interval=1, 
                    fontsize=16,
                    foreground = colors[3],
                    background = colors[0],
                    func=lambda: subprocess.check_output(os.path.expanduser("~/.local/bin/statusbar/idleinhibit")).decode(), 
                    mouse_callbacks={'Button1': lambda: qtile.spawn(os.path.expanduser("~/.local/bin/statusbar/idleinhibit toggle"), 
                    shell=True)} 
                    ),                 
                widget.Sep(
                    linewidth=0,
                    foreground=colors[1],
                    padding=5,
                    size_percent=50,
                ),
            ],
            24,
            #background="#000000AA",
            background= colors[0],
            #margin=[4, 9, 4, 9],          
            margin=[0, 0, 0, 0],
            # Margin = N E S W
        ),
        bottom=bar.Gap(5),
        left=bar.Gap(5),
        right=bar.Gap(5),
    ),
]
