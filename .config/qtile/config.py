import os
import subprocess

from colors import colors
from screens import screens
#from keys import mod, keys

from libqtile.config import (
    # KeyChord,
    Key,
    # Screen,
    Group,
    Drag,
    Click,
    ScratchPad,
    DropDown,
    Match,
)
from libqtile import extension
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.backend.wayland.inputs import InputConfig
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration

# # # Configure input devices

# wl_input_rules = {
#     "type:keyboard": InputConfig(kb_layout="us"),
#     "2:7:SynPS/2_Synaptics_TouchPad": InputConfig(click_method="button_areas"),
# }

mod = "mod4"
# terminal = "kitty"
terminal = "foot"

# Resize functions for bsp layout
def resize(qtile, direction):
    layout = qtile.current_layout
    child = layout.current
    parent = child.parent

    while parent:
        if child in parent.children:
            layout_all = False

            if (direction == "left" and parent.split_horizontal) or (
                direction == "up" and not parent.split_horizontal
            ):
                parent.split_ratio = max(5, parent.split_ratio - layout.grow_amount)
                layout_all = True
            elif (direction == "right" and parent.split_horizontal) or (
                direction == "down" and not parent.split_horizontal
            ):
                parent.split_ratio = min(95, parent.split_ratio + layout.grow_amount)
                layout_all = True

            if layout_all:
                layout.group.layout_all()
                break

        child = parent
        parent = child.parent


@lazy.function
def resize_left(qtile):
    resize(qtile, "left")


@lazy.function
def resize_right(qtile):
    resize(qtile, "right")


@lazy.function
def resize_up(qtile):
    resize(qtile, "up")


@lazy.function
def resize_down(qtile):
    resize(qtile, "down")


keys = [
    # Rofi
    Key(
        [mod],
        "c",
        lazy.spawn("rofi -show drun"),
        desc="Launch Rofi menu",
    ),
    Key(
        [mod],
        "d",
        lazy.spawn("fuzzel"),
        desc="Launch Fuzzel menu",
    ),
    Key(
        [mod],
        "Return",
        lazy.spawn(terminal),
        desc="Launch terminal",
    ),
    Key(
        [mod, "shift"],
        "Return",
        lazy.group["scratchpad"].dropdown_toggle("terminal")),
    Key(
        [mod, "shift"],
        "p",
        lazy.spawn("/home/jeroen/.local/bin/powermenu.sh"),
        desc="Power Screen",
    ),
    Key([mod], "x",
        lazy.spawn("swaylock -f -i /home/jeroen/.cache/wallpaper"),
        desc="Lock screen"
        ),
    Key(
        [mod],
        "Tab",
        lazy.next_layout(),
        desc="Toggle through layouts",
    ),
    Key(
        [mod],
        "q",
        lazy.window.kill(),
        desc="Kill focused window",
    ),
    Key(
        [mod, "shift"],
        "r",
        lazy.reload_config(),
        desc="Restart Qtile",
    ),
    Key(
        [mod, "shift"],
        "q",
        lazy.shutdown(),
        desc="Shutdown Qtile",
    ),
    # Window controls
    Key(
        [mod],
        "j",
        lazy.layout.down(),
        desc="Move focus down in current stack pane",
    ),
    Key(
        [mod],
        "k",
        lazy.layout.up(),
        desc="Move focus up in current stack pane",
    ),
    Key(
        [mod],
        "h",
        lazy.layout.left(),
        lazy.layout.next(),
        desc="Move focus left in current stack pane",
    ),
    Key(
        [mod],
        "l",
        lazy.layout.right(),
        lazy.layout.previous(),
        desc="Move focus right in current stack pane",
    ),
    Key(
        [mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        desc="Move windows down in current stack",
    ),
    Key(
        [mod, "shift"],
        "k",
        lazy.layout.shuffle_up(),
        desc="Move windows up in current stack",
    ),
    Key(
        [mod, "shift"],
        "h",
        lazy.layout.shuffle_left(),
        lazy.layout.swap_left(),
        lazy.layout.client_to_previous(),
        desc="Move windows left in current stack",
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        lazy.layout.swap_right(),
        lazy.layout.client_to_next(),
        desc="Move windows right in the current stack",
    ),
    Key(
        [mod, "control"],
        "j",
        lazy.layout.flip_down(),
        desc="Flip layout down",
    ),
    Key(
        [mod, "control"],
        "k",
        lazy.layout.flip_up(),
        desc="Flip layout up",
    ),
    Key(
        [mod, "control"],
        "h",
        lazy.layout.flip_left(),
        desc="Flip layout left",
    ),
    Key(
        [mod, "control"],
        "l",
        lazy.layout.flip_right(),
        desc="Flip layout right",
    ),
    Key(
        [mod],
        "u",
        resize_left,
        desc="Resize window left",
    ),
    Key(
        [mod],
        "i",
        resize_right,
        desc="Resize window Right",
    ),
    Key(
        [mod],
        "o",
        resize_up,
        desc="Resize windows upward",
    ),
    Key(
        [mod],
        "p",
        resize_down,
        desc="Resize windows downward",
    ),
    Key(
        [mod],
        "n",
        lazy.layout.normalize(),
        desc="Normalize window size ratios",
    ),
    Key(
        [mod],
        "m",
        lazy.layout.maximize(),
        desc="Toggle window between minimum and maximum sizes",
    ),
    Key(
        [mod, "shift"],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen",
    ),
    Key(
        [mod],
        "t",
        lazy.window.toggle_floating(),
        desc="Toggle floating on focused window",
    ),
    # Audio bindings specifically for Logitech G915 media buttons
    Key(
        [],
        "XF86AudioNext",
        lazy.spawn("playerctl next"),
        desc="Play next audio",
    ),
    Key(
        [],
        "XF86AudioPlay",
        lazy.spawn("playerctl play-pause"),
        desc="Toggle play/pause audio",
    ),
    Key(
        [],
        "XF86AudioPrev",
        lazy.spawn("playerctl previous"),
        desc="Play previous audio",
    ),
    Key(
        [],
        "XF86AudioMute",
        lazy.spawn("amixer -q -D pulse sset Master toggle"),
        desc="Mute audio",
    ),
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +1%"),
        desc="Raise volume",
    ),
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -1%"),
        desc="Lower volume",
    ),
]

# Command to find out wm_class of window: xprop | grep WM_CLASS
workspaces = [
    {
        "name": "WEB",#" ",
        "key": "1",
        "label": "1",
        "matches": [Match(wm_class="firefox"), Match(wm_class="chromium"), Match(wm_class="brave-browser")],
        "layout": "monadtall",
        "spawn": ["Firefox"],
    },
    {
        "name": "MAIL",#" ",
        "key": "2",
        "label": "2",
        "matches": [Match(wm_class="Thunderbird"), Match(wm_class="thunderbird"), Match(wm_class="Mail")],
        "layout": "monadtall",
        "spawn": ["Thunderbird"],
    },
    {
        "name": "TERM",#" ",
        "key": "3",
        "label": "3",
        "matches": [Match(wm_class="")],
        "layout": "columns",
        "spawn": [],
    },
    {
        "name": "MEDIA",#"",
        "key": "4",
        "label": "4",
        "matches": [Match(wm_class="mpv"), Match(wm_class="gimp-2.10"), Match(wm_class="gtkpod")],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "DEV",#" ",
        "key": "5",
        "label": "5",
        "matches": [
            Match(wm_class="obsidian"),
            Match(wm_class="Emacs"),
            Match(wm_class="Code"),
            Match(wm_class="code-url-handler"),
        ],
        "layout": "monadtall",
        "spawn": ["Code"],
    },
    {
        "name": "FILES",#" ",
        "key": "6",
        "label": "6",
        "matches": [Match(wm_class="Org.gnome.Nautilus"), Match(wm_class="thunar"), Match(wm_class="Pcmanfm")],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "CHAT",#" ",
        "key": "7",
        "label": "7",
        "matches": [Match(wm_class="libreoffice"), Match(wm_class="discord")],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "GFX",#" ",
        "key": "8",
        "label": "8",
        "matches": [Match(wm_class="nwg-look")],
        "layout": "monadtall",
        "spawn": [],
    },
    {
        "name": "AUDIO",#" ",
        "key": "9",
        "label": "9",
        "matches": [Match(wm_class="blueman-manager")],
        "layout": "floating",
        "spawn": [],
    },
]

groups = []
for workspace in workspaces:
    matches = workspace["matches"] if "matches" in workspace else None
    layouts = workspace["layout"] if "layout" in workspace else None
    groups.append(Group(workspace["name"], matches=matches, layout=layouts))
    keys.append(Key([mod], workspace["key"], lazy.group[workspace["name"]].toscreen()))
    keys.append(
        Key([mod, "shift"], workspace["key"], lazy.window.togroup(workspace["name"]))
    )

layout_theme = {
    "border_width": 0,
    "margin": 5,
    "border_focus": "3b4252",
    "border_normal": "3b4252",
    "font": "Roboto",
    "grow_amount": 4,
}

layout_theme_margins = {
    "name": "bsp-margins",
    "border_width": 1,
    "margin": [150, 300, 150, 300],
    "border_focus": "3b4252",
    "border_normal": "3b4252",
    "font": "Roboto", #"FiraCode Nerd Font",
    "grow_amount": 4,
}

layout_audio = {
    "name": "monadwide-audio",
    "border_width": 1,
    "margin": 100,
    "border_focus": "3b4252",
    "border_normal": "3b4252",
    "font": "Roboto", #"FiraCode Nerd Font",
    "ratio": 0.65,
    "new_client_position": "after_current",
}

layouts = [
    layout.MonadWide(**layout_audio),
    layout.Bsp(**layout_theme, fair=False),
    layout.Columns(**layout_theme),
    layout.Matrix(**layout_theme, columns=3),
    layout.MonadTall(**layout_theme),
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme),    
    layout.TreeTab(
         font = "Roboto",
         fontsize = 10,
         sections = ["FIRST", "SECOND", "THIRD", "FOURTH"],
         section_fontsize = 10,
         border_width = 2,
         bg_color = "1c1f24",
         active_bg = "c678dd",
         active_fg = "000000",
         inactive_bg = "a9a1e1",
         inactive_fg = "1c1f24",
         padding_left = 0,
         padding_x = 0,
         padding_y = 5,
         section_top = 10,
         section_bottom = 20,
         level_shift = 8,
         vspace = 3,
         panel_width = 200
         ),
]

floating_layout = layout.Floating(
    **layout_theme,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        Match(title='Quit and close tabs?'),
        Match(wm_type='utility'),
        Match(wm_type='notification'),
        Match(wm_type='toolbar'),
        Match(wm_type='splash'),
        Match(wm_type='dialog'),
        Match(wm_class='gimp-2.99'),
        #Match(wm_class='Firefox'),
        Match(wm_class='file_progress'),
        Match(wm_class='confirm'),
        Match(wm_class='dialog'),
        Match(wm_class='download'),
        Match(wm_class='error'),
        Match(wm_class='notification'),
        Match(wm_class='splash'),
        Match(wm_class='toolbar'),
        Match(title='About LibreOffice'),
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
    ],
)


# Setup bar

widget_defaults = dict(
    font="RobotoMono",
    fontsize="11",
    padding=2,
)
extension_defaults = widget_defaults.copy()


# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

wl_input_rules = {
    "1739:33391:SYNA3063:00 06CB:826F Touchpad": InputConfig(natural_scroll=True),
}

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = "floating_only"
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "focus"

# Append a scratchpad group

conf = {
    "warp_pointer": False,
    "on_focus_lost_hide": True,
    "opacity": 0.90,
}

groups.append(
    ScratchPad(
        "scratchpad", [
            # Define a drop down terminal
            # it is placed in the upper third of the screen by default
            DropDown(
                "terminal",
                terminal + " -a 'Terminal'",
                height=0.50,
                width=0.50,
                x=0.25,
                y=0.2,
                **conf
            ),

        ]
    )
)

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.call([home])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "Qtile"
