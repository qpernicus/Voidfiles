set -g fish_greeting
#set TERM xterm-256color # Sets the terminal type
set TERM xterm-kitty
#set fish_function_path $fish_function_path "usr/share/powerline/bindings/fish"
#source /usr/share/powerline/bindings/fish/powerline-setup.fish
#powerline-setup
set -x SSH_ASKPASS ~/.local/bin/ssh-askpass
# Enable the use of ssh-agent
set -x SSH_AUTH_SOCK "$XDG_RUNTIME_DIR/ssh-agent.socket"


### SET MANPAGER
### Uncomment only one of these!

### "bat" as manpager
#set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"
set -x MANPAGER "/home/jeroen/.local/bin/batwrap.sh"
### "vim" as manpager
#set -x MANPAGER '/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'

### "nvim" as manpager
#set -x MANPAGER "nvim -c 'set ft=man' -"

### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
# set fish_color_normal brcyan
# set fish_color_autosuggestion '#7d7d7d'
# set fish_color_command brcyan
# set fish_color_error '#ff6c6b'
# set fish_color_param brcyan
alias v='nvim'
alias vi='nvim'
alias vim='nvim'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first' # all files and dirs
alias ll='exa -l --color=always --group-directories-first' # long format
alias l.='exa -a | egrep "^\."'

#  bare git repo alias for dotfiles
alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"

# Updates
alias install='sudo xbps-install'
alias update='sudo xbps-install -Su'
alias search='xbps-query -Rs'
alias src-install='sudo xbps-install --repository='
alias build='./xbps-src pkg'
alias remove='sudo xbps-remove -Rov'
alias cleanup='sudo xbps-remove -yO' #Clear package cache
alias cleanorphan='sudo xbps-remove -yo' #Remove Orphans
alias purge='sudo vkpurge rm all' #Purge Kernels
# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias xprop='swaymsg -t get_tree | less'
alias class='sleep 3 && qtile cmd-obj -o window -f eval -a "self.get_wm_class()"'


set -l xdg_data_home $XDG_DATA_HOME ~/.local/share
set -gx --path XDG_DATA_DIRS $xdg_data_home[1]/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share

for flatpakdir in ~/.local/share/flatpak/exports/bin /var/lib/flatpak/exports/bin
    if test -d $flatpakdir
        contains $flatpakdir $PATH; or set -a PATH $flatpakdir
    end
end



# Running starship
starship init fish | source
